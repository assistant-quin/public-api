FROM docker.io/hairyhenderson/gomplate AS bin

# ---
FROM docker.io/alpine as build

WORKDIR /build
COPY --from=bin /gomplate /build/gomplate
ARG ENV=dev

ADD .gomplate.yaml krakend.tmpl postman.tmpl /build/
ADD settings /build/settings
ADD templates /build/templates

RUN ./gomplate -c env=settings/${ENV}/env.yaml

# ---

FROM docker.io/devopsfaith/krakend as krakend
COPY --from=build /build/krakend.json /etc/krakend/krakend.json

# ---
FROM golang:alpine as postman

WORKDIR /build
COPY --from=build /build/postman.json /build/postman.json

RUN go install github.com/thedevsaddam/docgen@latest
