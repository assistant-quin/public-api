# Quin - Public API

## API Gateway

An API Gateway is a service that receives calls from the outside world and translates them to calls to our internal services. This allows internal organization to be independent of public contract, which can then stay consistent.

Because the gateway has the capacity to make several internal calls to answer one request, it also allows much cleaner/simpler services. Finally, it also allows to not pollute every service with authorization checks.

## How this repository works

We use the [KrakenD API Gateway](https://www.krakend.io/open-source/) in its open source version.

It is really easy to run as one only needs to properly make one configuration file.
To allow using it in different environments (dev, prod, ..), we build it using a templating system: Gomplate.

The flow is as follows:

- Developer makes changes on the templates
- (optional but not really) Developer builds the gateway to check that everything is fine
- Developer pushes on a branch
- CI builds the gateway, checks that the file is valid and builds the public documentation (all of that is upcoming)
- Reviewer reviews and merges
- CD builds the gateway of each environment, tags them and deploys the updated version to production (all of that is upcoming)

### How to work with this repository

```bash
# clone the repo
git clone $repoSSHUrl public-api
cd public-api

# build and run the containers
docker-compose up

# access the API
# (upcoming)

# access the API documentation
# open http://localhost:8000

# stop the containers
docker-compose down
```

## Templating system - Gomplate

→ [Documentation](https://docs.gomplate.ca/)

- All settings are in settings/_environment_. So for example, dev settings are in settings/dev, production settings in settings/production. They can be accessed through `.env.some_key`.
- **Do not commit secrets**.
- Make use of the templates to keep everything readable.

## KrakenD

→ [General Documentation](https://www.krakend.io/docs/overview/)
→ [File Format Documentation](https://www.krakend.io/docs/configuration/structure/)

- The `krakend.tmpl` file gets parsed by Gomplate and generates a `krakend.json` file.
- It is on purpose that the `krakend.json` file does not get commited.
- The generated `krakend.json` file is used to run our Public API Gateway.

## Postman

→ [File Format Documentation](https://learning.postman.com/collection-format/getting-started/structure-of-a-collection/)

- The `postman.tmpl` file gets parsed by Gomplate and generates a `postman.json` file.
- It is on purpose that the `postman.json` file does not get commited.
- The generated `postman.json` file is used to generate the Public API Documentation.
